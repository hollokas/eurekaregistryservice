package com.crypto.eurekaregistryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class EurekaregistryserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaregistryserviceApplication.class, args);
	}
}
